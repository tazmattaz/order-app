import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import { useSelector } from 'react-redux';
import {
  BrandProducts,
  BrandProducts2,
  Brands,
  Brands2,
  CMSLinks,
  ChatRoom,
  ChatRoomForVendor,
  ChatScreen,
  ChatScreenForVendor,
  P2pChatRoom,
  P2pChatScreen,
  ProductDetail,
  ProductDetail2,
  ProductList2,
  ProductList3,
  ProductList4,
  ProductListEcom,
  WebLinks,
  WebviewScreen
} from '../Screens';
import AppIntro from '../Screens/AppIntro';
import ShortCode from '../Screens/ShortCode/ShortCode';
import AuthStack from './AuthStack';
import CourierStack from './CourierStack';
import DrawerRoutes from './DrawerRoutes';
import { navigationRef } from './NavigationService';
import TabRoutesVendor from './TabRoutesVendor';
import TaxiAppStack from './TaxiAppStack';
import TabRoutesVendorNewTemplate from './VendorApp/TabRoutesVendor';
import navigationStrings from './navigationStrings';
import TabRoutesP2pOnDemand from './TabRoutesP2pOnDemand';
import TabRoutes from './TabRoutes';
import TaxiTabRoutes from './TaxiTabRoutes';
import TabRoutesP2p from './TabRoutesP2p';
import TabRoutesEcommerce from './TabRoutesEcommerce';


const Stack = createNativeStackNavigator();


export default function Routes() {
  const { userData, appSessionInfo } = useSelector((state) => state?.auth || {});
  const { appStyle, themeColors, appData } = useSelector((state) => state?.initBoot || {});
  const businessType = appStyle?.homePageLayout;
  console.log('businessType',businessType);
  const renderProductDetailsScreens = () => {
    switch (appStyle?.homePageLayout) {
      case 2:
        return ProductDetail2;
      default:
        return ProductDetail;
    }
  };
  const renderProductListScreen = () => {
    switch (appStyle?.homePageLayout) {
      case 1: return ProductList4;
      case 2: return ProductList2;
      case 10: return ProductListEcom;
      default: return ProductList3;
    }
  };
  const renderBrandProductsScreens = () => {
    switch (appStyle?.homePageLayout) {
      case 1:
        return BrandProducts;
      default:
        return BrandProducts2;
    }
  };

  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={{ headerShown: false }}>

        {appSessionInfo == 'shortcode' ||
          appSessionInfo == 'show_shortcode' ? (

          <Stack.Screen
            name={navigationStrings.SHORT_CODE}
            component={ShortCode}
          />

        ) : appSessionInfo == 'app_intro' ? (
          <Stack.Screen
            name={navigationStrings.APP_INTRO}
            component={AppIntro}
            options={{ gestureEnabled: false }}
          />
        ) : appSessionInfo == 'guest_login' || !!userData?.auth_token ? (
          <React.Fragment>
            {
              businessType === 10 ? <Stack.Screen
                name={navigationStrings.DRAWER_ROUTES}
                component={DrawerRoutes}
                options={{ gestureEnabled: false }}
              /> : <Stack.Screen
                name={navigationStrings.TAB_ROUTES}
                component={
                 TabRoutes
                }
                options={{ gestureEnabled: false }}
              />
            }</React.Fragment>



        ) : (
          AuthStack(Stack, appStyle, appData)
        )}

        {CourierStack(Stack)}

        {TaxiAppStack(Stack)}

        <Stack.Screen
          name={navigationStrings.CHAT_SCREEN}
          component={!!appData?.profile?.preferences?.is_rental_weekly_monthly_price ? P2pChatScreen : ChatScreen}
        />
        <Stack.Screen
          name={navigationStrings.CHAT_SCREEN_FOR_VENDOR}
          component={ChatScreenForVendor}
        />
        <Stack.Screen
          name={navigationStrings.CHAT_ROOM}
          component={!!appData?.profile?.preferences?.is_rental_weekly_monthly_price ? P2pChatRoom : ChatRoom}
        />
        <Stack.Screen
          name={navigationStrings.CHAT_ROOM_FOR_VENDOR}
          component={ChatRoomForVendor}
        />

        <Stack.Screen
          name={navigationStrings.TABROUTESVENDOR}
          component={TabRoutesVendor}
          options={{ gestureEnabled: false }}
        />
        <Stack.Screen
          name={navigationStrings.TABROUTESVENDORNEW}
          component={TabRoutesVendorNewTemplate}
          options={{ gestureEnabled: false }}
        />
        <Stack.Screen
          name={navigationStrings.WEBLINKS}
          component={WebLinks}
          options={{ headerShown: false }}
        />
          <Stack.Screen
        name={navigationStrings.WEBVIEWSCREEN}
        component={WebviewScreen}
      />
        <Stack.Screen
        name={navigationStrings.PRODUCT_LIST}
        component={renderProductListScreen()}
      />
        <Stack.Screen
        name={navigationStrings.PRODUCTDETAIL}
        component={renderProductDetailsScreens()}
      />
       
        <Stack.Screen
          name={navigationStrings.CMSLINKS}
          component={CMSLinks}
          options={{ headerShown: false }}
        />
          <Stack.Screen
        component={
          appStyle?.homePageLayout === 3 || appStyle?.homePageLayout === 5
            ? Brands2
            : Brands
        }
        name={navigationStrings.BRANDS}
      />
      <Stack.Screen
      name={navigationStrings.BRANDDETAIL}
        component={renderBrandProductsScreens()}
      />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

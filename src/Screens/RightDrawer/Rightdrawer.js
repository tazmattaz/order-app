import { View, Text, Image, TouchableOpacity, SafeAreaView, I18nManager } from 'react-native'
import React, { useState } from 'react'
import WrapperContainer from '../../Components/WrapperContainer'
import DashBoardHeaderOne from '../Home/DashboardViews/DashBoardHeaderOne'
import Header from '../../Components/Header'
import Header2 from '../../Components/Header2'
import Header3 from '../../Components/Header3'
import imagePath from '../../constants/imagePath'
import { moderateScale, moderateScaleVertical, textScale, width } from '../../styles/responsiveSize'
import { FlatList, ScrollView } from 'react-native-gesture-handler'
import HorizontalLine from '../../Components/HorizontalLine'
import fontFamily from '../../styles/fontFamily'
import { colors } from 'react-native-elements'
import { getImageUrl } from '../../utils/helperFunctions'
import { useSelector } from 'react-redux'
import { useDarkMode } from 'react-native-dynamic'
import { Accordion } from 'react-native-paper/lib/typescript/components/List/List'
import navigationStrings from '../../navigation/navigationStrings'
import DropDownPicker from 'react-native-dropdown-picker'
import { setItem } from '../../utils/utils'
import actions from '../../redux/actions'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { changeLaguage } from '../../constants/lang'
import { BluetoothManager } from '@brooons/react-native-bluetooth-escpos-printer'
import { useFocusEffect } from '@react-navigation/native'
import Loader from '../../Components/Loader'
import RNRestart from 'react-native-restart';
import staticStrings from '../../constants/staticStrings'

const Rightdrawer = ({ navigation }) => {
    const updateState = (data) => setState((state) => ({ ...state, ...data }));
    const {
        currencies,
        languages,
       
      } = useSelector((state) => state?.initBoot);
      
      useFocusEffect(
        React.useCallback(() => {
          updateState({
            appCurrencies: currencies,
            appLanguages: languages,
          });
        }, [currencies, languages]),
      );

    const appMainData = useSelector((state) => state?.home?.appMainData || {});
    const { dineInType } = useSelector((state) => state?.home || {});
    const userData = useSelector((state) => state.auth.userData);
    const [state, setState] = useState({
        isLoading: false,
        country: 'uk',
        appCurrencies: currencies,
        appLanguages: languages,
        isOn: false,
        
        selectedThemeOption: null,
      });
    
      const {
        isLoading,
        appCurrencies,
        appLanguages,
        isOn,
        selectedThemeOptions,
        selectedThemeOption,
      } = state;
    console.log(appMainData, 'appMainData');
    const { appData, themeColors, appStyle } = useSelector(
        (state) => state?.initBoot,
    );
    const theme = useSelector((state) => state?.initBoot?.themeColor);
    const toggleTheme = useSelector((state) => state?.initBoot?.themeToggle);
    const darkthemeusingDevice = useDarkMode();
    const profileInfo = appData?.profile;
    const isDarkMode = toggleTheme ? darkthemeusingDevice : theme;
    const imageURI = getImageUrl(
        isDarkMode
            ? profileInfo?.dark_logo?.image_fit
            : profileInfo?.logo?.image_fit,
        isDarkMode
            ? profileInfo?.dark_logo?.image_path
            : profileInfo?.logo?.image_path,
        '1000/1000',
    );
    const [subcategory, setsubcategory] = useState()
   const[showcurrency,setcurrency]=useState()
    const[showlanguage,setshowlanguage]=useState()
    var catagories = appMainData?.homePageLabels?.filter(i => i?.slug == 'nav_categories')
    const updateLanguage = (item) => {
        console.log(item, 'itemmmm');
        const data = languages.all_languages.filter((x) => x.id == item.id)[0];
        // console.log(data, "setLang")
        if (data.sort_code !== languages.primary_language.sort_code) {
          let languagesData = {
            ...languages,
            primary_language: data,
          };
          // updateState({isLoading: true});
          setItem('setPrimaryLanguage', languagesData);
          setTimeout(() => {
            // updateState({ isLoading: false });
            actions.updateLanguage(data);
            onSubmitLang(data.sort_code, languagesData);
          }, 1000);
        }
      };
      const updateCurrency = (item) => {
        const data = currencies.all_currencies.filter((x) => x.id == item.id)[0];
    
        if (data.iso_code !== currencies.primary_currency.iso_code) {
          let currenciesData = {
            ...currencies,
            primary_currency: data,
          };
          setItem('setPrimaryCurrent', currenciesData);
          updateState({ isLoading: true });
          setTimeout(() => {
            updateState({ isLoading: false });
            actions.updateCurrency(data);
          }, 1000);
        }
      };

      const onPressCategory = (item) => {
        console.log(item,'dhdhhdhdhdhhdhd')
        if (!!appData?.profile?.preferences?.is_service_product_price_from_dispatch && priceType == "vendor" && dineInType === "on_demand" && appStyle?.homePageLayout == 9 && !!appData?.profile?.preferences?.is_service_price_selection) {
          navigation.navigate(navigationStrings.PRODUCT_LIST, {
            fetchOffers: true,
            id: item.id,
            vendor:
              item.redirect_to == staticStrings.ONDEMANDSERVICE ||
                item.redirect_to == staticStrings.PRODUCT ||
                item?.redirect_to == staticStrings.LAUNDRY ||
                item?.redirect_to == staticStrings.APPOINTMENT ||
                item?.redirect_to == staticStrings.RENTAL
                ? false
                : true,
            name: item.name,
            isVendorList: false,
          });
          return
        }
    
        if (dineInType === "on_demand" && appStyle?.homePageLayout == 9) {
    
          navigation.navigate(navigationStrings.FREELANCER_SERVICE, {
            fetchOffers: true,
            id: item.id,
            vendor: false,
            name: item.name,
            isVendorList: false,
          });
          return
        }
    
        if (item?.redirect_to == staticStrings.P2P) {
          navigation.navigate(navigationStrings.P2P_PRODUCTS, item);
          return;
        }
        if (item?.redirect_to == staticStrings.FOOD_TEMPLATE) {
          navigation.navigate(navigationStrings.SUBCATEGORY_VENDORS, item);
          return;
        }
        if (item?.redirect_to == staticStrings.SUBCATEGORY && appStyle?.homePageLayout == 10) {
          navigation.navigate(navigationStrings.SUBCATEGORY_VENDORS, item);
          return;
        }
    
    
        if (item.redirect_to == staticStrings.VENDOR) {
          navigation.navigate(navigationStrings.VENDOR, item);
        } else if (
          item.redirect_to == staticStrings.PRODUCT ||
          item.redirect_to == staticStrings.CATEGORY ||
          item.redirect_to == staticStrings.ONDEMANDSERVICE ||
          item?.redirect_to == staticStrings.LAUNDRY
        ) {
          navigation.navigate(navigationStrings.PRODUCT_LIST, {
            fetchOffers: true,
            id: item.id,
            vendor:
              item.redirect_to == staticStrings.ONDEMANDSERVICE ||
                item.redirect_to == staticStrings.PRODUCT ||
                item?.redirect_to == staticStrings.LAUNDRY
                ? false
                : true,
            name: item.name,
            isVendorList: false,
          });
        } else if (item.redirect_to == staticStrings.PICKUPANDDELIEVRY) {
          if (!!userData?.auth_token) {
            if (shortCodes.arenagrub == appData?.profile?.code) {
              openUber();
            } else {
              item['pickup_taxi'] = true;
              navigation.navigate(navigationStrings.ADDADDRESS, item);
            }
          } else {
            actions.setAppSessionData('on_login');
          }
        } else if (item.redirect_to == staticStrings.DISPATCHER) {
          // navigation.navigate(navigationStrings.DELIVERY, item)();
        } else if (item.redirect_to == staticStrings.CELEBRITY) {
          navigation.navigate(navigationStrings.CELEBRITY);
        } else if (item.redirect_to == staticStrings.BRAND) {
          navigation.navigate(navigationStrings.CATEGORY_BRANDS, item);
        } else if (item.redirect_to == staticStrings.SUBCATEGORY) {
          // navigation.navigate(navigationStrings.PRODUCT_LIST, item)();
          navigation.navigate(navigationStrings.VENDOR_DETAIL, { item });
        } else if (!item.is_show_category || item.is_show_category) {
          item?.is_show_category
            ? navigation.navigate(navigationStrings.VENDOR_DETAIL, {
              item,
              rootProducts: true,
              // categoryData: data,
            })
            : navigation.navigate(navigationStrings.PRODUCT_LIST, {
              id: item?.id,
              vendor: true,
              name: item?.name,
              isVendorList: true,
              fetchOffers: true,
            });
          // navigation.navigate(navigationStrings.VENDOR_DETAIL, {item})();
        }
      };
      const onSubmitLang = async (lang, languagesData) => {
        if (lang == '') {
          showAlertMessageError(strings.SELECT);
          return;
        } else {
          let btData = {};
          AsyncStorage.getItem('BleDevice').then(async (res) => {
            if (res !== null) {
              btData = res;
              await AsyncStorage.setItem('autoConnectEnabled', 'true');
              await AsyncStorage.setItem('BleDevice2', btData);
              console.log('++++++22', btData);
              if (lang === 'ar' || lang === 'he') {
                I18nManager.forceRTL(true);
                setItem('language', lang);
                changeLaguage(lang);
                RNRestart.Restart();
              } else {
                I18nManager.forceRTL(false);
                setItem('language', lang);
                changeLaguage(lang);
                RNRestart.Restart();
              }
              BluetoothManager.disconnect(JSON.parse(res).boundAddress).then(
                (s) => { },
              );
            } else {
              if (lang === 'ar' || lang === 'he') {
                I18nManager.forceRTL(true);
                setItem('language', lang);
                changeLaguage(lang);
                RNRestart.Restart();
              } else {
                I18nManager.forceRTL(false);
                setItem('language', lang);
                changeLaguage(lang);
                RNRestart.Restart();
              }
            }
          });
          // await BackgroundService.removeAllListeners();
          // await BackgroundService.stop().then((res) => {});
          // await AsyncStorage.removeItem('BleDevice');
        }
      };

    console.log(catagories, 'jhfgasjdfyafd')
   
    return (
        <SafeAreaView style={{ backgroundColor: colors.white ,flex:1}}>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: moderateScale(24), marginVertical: moderateScaleVertical(16), alignItems: 'center' }}>
                <Image
                    style={{ height: moderateScaleVertical(62), width: moderateScale(60), resizeMode:'contain' }}
                    source={{
                        uri: imageURI,
                    }}
                />
                {/* <View style={{ flexDirection: 'row', alignItems: 'center' }}> */}
                    {/* <TouchableOpacity
                        activeOpacity={1}
                    >
                        <Image
                            style={{}}
                            source={imagePath.search}
                        />
                    </TouchableOpacity> */}
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => navigation.goBack()}
                    >
                        <Image
                            style={{ marginLeft: 12 }}
                            source={imagePath.cross5}
                        />
                    </TouchableOpacity>
                {/* </View> */}
            </View>
            <ScrollView 
            showsVerticalScrollIndicator ={false}>
               { !!catagories[0].data ?  <FlatList
                    data={catagories[0].data}
                    renderItem={({ item, index }) => {
                        console.log(item, 'ksgdfswde')
                        return (<View style={{ marginHorizontal: moderateScale(24), marginTop: moderateScaleVertical(16) }}>
                            <TouchableOpacity onPress={() => {
                                if (subcategory == index) { setsubcategory(null) }
                                else { setsubcategory(index) }
                            }} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ color: 'black', fontSize: textScale(16), fontFamily: fontFamily.semiBold }}>{item.slug}</Text>
                                <Image source={imagePath.dropDownSingle} />

                            </TouchableOpacity>
                            <HorizontalLine lineStyle={{ marginTop: moderateScaleVertical(14), borderBottomWidth: 1 }}></HorizontalLine>

                            {subcategory == index && item.children.map((item1) => {
                                console.log(item1, 'kohoioio')
                                return (

                                    <TouchableOpacity onPress={()=>onPressCategory(item1)} style={{ backgroundColor:colors.white, padding: 10 }}>
                                        <Text style={{fontSize:textScale(14),fontFamily:fontFamily.semiBold}}>{item1.slug}</Text>
                                    </TouchableOpacity>)
                            }
                            )}

                        </View>)
                    }}
                />:<Loader/>}
 

                <View style={{ marginHorizontal: moderateScale(24) }}>
 
                     {/* {!userData?.auth_token && <TouchableOpacity onPress={()=>navigation.navigate(navigationStrings.LOGIN)} style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: moderateScaleVertical(16) }}>
                        <Text style={{ color: 'black', fontSize: textScale(16), fontFamily: fontFamily.semiBold }}>Login</Text>


                 </TouchableOpacity>}
                     <HorizontalLine lineStyle={{ marginTop: moderateScaleVertical(14), borderBottomWidth: 1 }}></HorizontalLine>  */}

                     {/* <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: moderateScaleVertical(16) }}>
                         <Text style={{ color: 'black', fontSize: textScale(16), fontFamily: fontFamily.semiBold }}>Register</Text>
                     </TouchableOpacity>
                     <HorizontalLine lineStyle={{ marginTop: moderateScaleVertical(14), borderBottomWidth: 1 }}></HorizontalLine>   */}

                   {!!userData?.auth_token && <TouchableOpacity onPress={()=>navigation.navigate(navigationStrings.WISHLIST)} style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: moderateScaleVertical(16) }}>
                        <Text style={{ color: 'black', fontSize: textScale(16), fontFamily: fontFamily.semiBold }}>Wishlist</Text>
                    </TouchableOpacity>}
                    {!!userData?.auth_token &&<HorizontalLine lineStyle={{ marginTop: moderateScaleVertical(14), borderBottomWidth: 1 }}></HorizontalLine>}

                  

                    <TouchableOpacity onPress={()=>setshowlanguage(!showlanguage)} style={{ flexDirection: 'row', marginTop: moderateScaleVertical(16) }}>
                        <Image source={imagePath.language_choice} style={{ height: moderateScaleVertical(30), width: moderateScale(30), tintColor: colors.black }} />
                        <Text style={{ color: 'black', fontSize: textScale(16), fontFamily: fontFamily.semiBold, marginLeft: moderateScale(8) }}>language</Text>
                    </TouchableOpacity>
                    <HorizontalLine lineStyle={{ marginTop: moderateScaleVertical(14), borderBottomWidth: 1 }}></HorizontalLine>
                    {  !!showlanguage &&<DropDownPicker
                items={appLanguages?.all_languages || []}
                defaultValue={
                  appLanguages?.primary_language?.nativeName ||
                  appLanguages?.primary_language?.name ||
                  appLanguages?.primary_language?.label ||
                  ''
                }
                
                containerStyle={{
                  height: 40,
                  marginTop: moderateScaleVertical(5),
                }}
                zIndex={1000}
                style={{
                  backgroundColor: isDarkMode
                    ? MyDarkTheme.colors.lightDark
                    : colors.greyColor1,
                  marginHorizontal: moderateScale(20),
                  flexDirection: 'row',
                  zIndex: 1000,
                }}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                labelStyle={{
                  color:  colors.textGrey,
                  textAlign: 'left',
                }}
                dropDownStyle={{
                  backgroundColor:'white',
                  minHeight: moderateScaleVertical(60),
                  maxHeight: moderateScaleVertical(145),
                  width: width - moderateScale(88),
                  alignSelf: 'center',
                  zIndex:1000
                }}
                arrowColor={ colors.black}
                onChangeItem={(item) => updateLanguage(item)}
              />}


<TouchableOpacity onPress={()=>setcurrency(!showcurrency)} style={{ flexDirection: 'row', marginTop: moderateScaleVertical(16) }}>
                        <Image source={imagePath.money} style={{ height: moderateScaleVertical(28), width: moderateScale(28), tintColor: colors.black }} />
                        <Text style={{ color: 'black', fontSize: textScale(16), fontFamily: fontFamily.semiBold, marginLeft: moderateScale(8) }}>currency</Text>
                    </TouchableOpacity>
                    <HorizontalLine lineStyle={{ marginTop: moderateScaleVertical(14), borderBottomWidth: 1 }}></HorizontalLine>

                   {!!showcurrency&& <DropDownPicker
                items={appCurrencies?.all_currencies || []}
                defaultValue={
                  appCurrencies?.primary_currency?.name ||
                  appCurrencies?.primary_currency?.label ||
                  ''
                }
                containerStyle={{ height: 40, marginTop: moderateScaleVertical(5) }}
                style={{
                  backgroundColor: isDarkMode
                    ? MyDarkTheme.colors.lightDark
                    : colors.greyColor1,
                  marginHorizontal: moderateScale(20),
                  flexDirection: 'row',
                }}
                labelStyle={{
                  color: isDarkMode ? MyDarkTheme.colors.text : colors.textGrey,
                  textAlign: 'left',
                }}
                itemStyle={{
                  justifyContent: 'flex-start',
                }}
                dropDownStyle={{
                  backgroundColor: 'white',
                  height: 120,
                  width: width - moderateScale(88),
                  alignSelf: 'center',
                  zIndex: 5000,
                }}
                zIndex={5000}
                arrowColor={isDarkMode ? MyDarkTheme.colors.text : colors.black}
                onChangeItem={(item) => updateCurrency(item)}
              />}
                


                </View>
                <View style={{ height: moderateScaleVertical(150) }}></View>
            </ScrollView>
        </SafeAreaView>
    )
}

export default Rightdrawer
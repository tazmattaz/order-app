import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import Carousel from 'react-native-snap-carousel';
import {
  height,
  moderateScale,
  moderateScaleVertical,
  width,
} from '../../../styles/responsiveSize';
import {getImageUrl} from '../../../utils/helperFunctions';
import FastImage from 'react-native-fast-image';
import DeviceInfo, {getBundleId} from 'react-native-device-info';
import colors from '../../../styles/colors';

const BannerView = ({
  item = {},
  appData = {},
  appMainData = {},
  appStyle = {},
  isDarkMode = false,
  themeColors = {},
  onBannerPress = () => {},
}) => {
  let myBanner =
    item?.banner_images ||
    appMainData?.mobile_banners ||
    appData?.mobile_banners ||
    [];

  console.log('banner data', myBanner);

  const [currentIndex, setIndex] = useState(0);

  const _onBannerPress = item => {
    onBannerPress(item);
  };

  const renderBanners = ({item = {}, index = 0}) => {
    const imageUrl =
      item?.banner_image_url ||
      getImageUrl(
        item?.image.image_fit,
        item?.image.image_path,
        appStyle?.homePageLayout === 5 ? '800/600' : '1200/1000',
      );

    return (
      <View 
      style={{}}
      key={String(item?.id || index)}>
        <TouchableOpacity
          style={{marginTop: moderateScaleVertical(10)}}
          activeOpacity={0.8}
          onPress={() => _onBannerPress(item)}>
          <FastImage
            source={{
              uri: imageUrl,
              priority: FastImage.priority.high,
              cache: FastImage.cacheControl.immutable,
            }}
            style={{
              height:
                appStyle?.homePageLayout !== 5
                  ? moderateScale(220)
                  : height / 3.8,
              width:
                appStyle?.homePageLayout !== 5 ? width : moderateScale(160),
              borderRadius: moderateScale(0),
              backgroundColor: isDarkMode
                ? colors.whiteOpacity15
                : colors.greyColor,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </TouchableOpacity>
      </View>
    );
  };
  return (
    <View>
      <Carousel
        // autoplay={true}
        // loop={true}
        autoplay={false}
        loop={false}
        // autoplayInterval={2000}
        data={myBanner}
        renderItem={renderBanners}
        sliderWidth={width}
        itemWidth={width}
        onSnapToItem={index => setIndex(index)}
      />

      <View style={styles.dotView}>
        {myBanner.map((va, i) => {
          return (
            <View
              style={{
                ...styles.bannerDotStyle,
                backgroundColor:
                  currentIndex === i
                    ? themeColors?.primary_color
                    : colors.borderColor,
              }}
            />
          );
        })}
      </View>
    </View>
  );
};

export default BannerView;

const styles = StyleSheet.create({
  dotView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: moderateScaleVertical(10),
    alignSelf: 'center',
  },
  bannerDotStyle: {
    height: moderateScale(8),
    width: moderateScale(8),
    borderRadius: moderateScale(4),
    marginRight: moderateScale(4),
  },
});

import { StyleSheet, Text, View } from "react-native";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useDarkMode } from "react-native-dynamic";
import { stylesFunc } from "./Header";
import { getBundleId } from "react-native-device-info";
import { appIds } from "../utils/constants/DynamicAppKeys";
import { TouchableOpacity } from "react-native";
import { Image } from "react-native";
import imagePath from "../constants/imagePath";
import { moderateScale, textScale } from "../styles/responsiveSize";
import colors from "../styles/colors";

const HeaderRed = ({
  navigation,
  goback = () => {},
  onPressDrawer = () => {},
  centerTittle = false,
  centerTittleText = "",
  rightIcon1 = {},
  rightIcon2 = {},
  rightIconTwoPress ={ },
  searchIcon = {},
  onPresssearch = () => {},
  rightIcons = false,
}) => {
  const { appData, themeColors, appStyle } = useSelector(
    (state) => state?.initBoot
  );
  const theme = useSelector((state) => state?.initBoot?.themeColor);
  const toggleTheme = useSelector((state) => state?.initBoot?.themeToggle);
  const darkthemeusingDevice = useDarkMode();
  const isDarkMode = toggleTheme ? darkthemeusingDevice : theme;

  const fontFamily = appStyle?.fontSizeData;
  const styles = stylesFunc({ themeColors, fontFamily });
  // const styles = stylesFunc({ themeColors, fontFamily });
  //update state
  return (
    <View
      style={{
        marginHorizontal: moderateScale(12),
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems:'center',
        marginTop: Platform.OS === "ios" ? null : 10,
      }}
    >
      <TouchableOpacity
        activeOpacity={1}
        onPress={goback}
        style={{ alignItems: "center" }}
      >
        <Image
          style={{
              tintColor: isDarkMode?colors.white:colors.black,
            marginRight: moderateScale(16),
            height: moderateScale(24),
            width: moderateScale(24),
            
          }}
          source={rightIcon1}
          resizeMode="contain"
        />
      </TouchableOpacity>
      {centerTittle && (
        <Text
          numberOfLines={1}
          style={{ fontFamily: fontFamily.bold,
             fontSize: textScale(16)
          ,color:isDarkMode?colors.white:colors.black }}
        >
          {centerTittleText}
        </Text>
      )}
      <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        {searchIcon && (
          <TouchableOpacity onPress={onPresssearch}>
            <Image
              style={{
                marginLeft: moderateScale(12),
                tintColor: isDarkMode?colors.white:colors.black,
              }}
              source={searchIcon}
            />
          </TouchableOpacity>
        )}
        {rightIcon2 && (
          <TouchableOpacity
            // activeOpacity={1}
            onPress={()=>rightIconTwoPress()}
          >
            <Image
              style={{
                marginLeft: moderateScale(12),
                tintColor:  isDarkMode?colors.white:colors.black,
              }}
              source={rightIcon2}
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default HeaderRed;

const styles = StyleSheet.create({});

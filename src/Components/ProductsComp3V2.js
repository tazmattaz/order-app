import React, {useState} from 'react';
import {
  Animated,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useDarkMode} from 'react-native-dynamic';
import FastImage from 'react-native-fast-image';
import {useSelector} from 'react-redux';
import imagePath from '../constants/imagePath';
import strings from '../constants/lang';
import colors from '../styles/colors';
import {
  moderateScale,
  moderateScaleVertical,
  textScale,
  width,
} from '../styles/responsiveSize';
import {MyDarkTheme} from '../styles/theme';
import {
  getImageUrlNew,
  tokenConverterPlusCurrencyNumberFormater,
} from '../utils/commonFunction';
import {
  getImageUrl,
  getScaleTransformationStyle,
  pressInAnimation,
  pressOutAnimation,
  showError,
  showSuccess,
} from '../utils/helperFunctions';
import actions from '../redux/actions';
let imageHeight = moderateScaleVertical(190);
let imageWidth = moderateScale(145);
let imageRadius = moderateScale(6);

const ProductsComp = ({
  isDiscount,
  item,
  imageStyle,
  onPress = () => {},
  numberOfLines = 1,
  containerStyle = {},
  priceType = 'vendor',
}) => {
  const {themeColors, appStyle, currencies, themeColor, themeToggle} =
    useSelector(state => state?.initBoot || {});
  const {additional_preferences, digit_after_decimal} = useSelector(
    state => state?.initBoot?.appData?.profile?.preferences || {},
  );
  const darkthemeusingDevice = useDarkMode();
  const userData = useSelector(state => state?.auth?.userData);
  const {appData, languages} = useSelector(state => state?.initBoot);
  const isDarkMode = themeToggle ? darkthemeusingDevice : themeColor;
  const fontFamily = appStyle?.fontSizeData;
  const scaleInAnimated = new Animated.Value(0);
  const [productdetail, setproductdetail] = useState();

  const appMainData = useSelector(state => state?.home?.appMainData || {});
  console.log('itemmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm', item);
  //add Product to wishlist
  const _onAddtoWishlist = item => {
    console.log(item, 'itemwishlist');
    if (!!userData?.auth_token) {
      actions
        .updateProductWishListData(
          `/${item.id}`,
          {},
          {
            code: appData?.profile?.code,
            currency: currencies?.primary_currency?.id,
            language: languages?.primary_language?.id,
          },
        )
        .then(res => {
          showSuccess(res.message);

          if (item.inwishlist) {
            item.inwishlist = null;
            updateState({productDetailData: item});
          } else {
            item.inwishlist = {product_id: item.id};
            updateState({productDetailData: item});
          }
        })
        .catch();
    } else {
      showError(strings.UNAUTHORIZED_MESSAGE);
    }
  };
  const {category = {}} = item || {};
  const touchableHitSlopProp = {
    // top: 10,
    // right: 30,
    // left: 40,
    // bottom: 40,
  };
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={{
         backgroundColor: isDarkMode ? colors.whiteOpacity15 : colors.white,
        // justifyContent:'center',
        // backgroundColor:'red',
        alignItems: 'center',
        width: imageWidth,
        height: moderateScaleVertical(243),
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 1, },
        // shadowOpacity: 0.1,
        // shadowRadius: 2,
        // elevation: 2,
        // margin: 1,
        borderRadius: imageRadius,
        ...containerStyle,
        ...getScaleTransformationStyle(scaleInAnimated),
      }}
      onPressIn={() => pressInAnimation(scaleInAnimated)}
      onPressOut={() => pressOutAnimation(scaleInAnimated)}>
      <View>
        <FastImage
          resizeMode={FastImage.resizeMode.cover}
          source={{
            uri: getImageUrlNew({
              url: item?.path || null,
              image_const_arr: appMainData.image_prefix,
              type: 'image_fill',
            }),
            cache: FastImage.cacheControl.immutable,
            priority: FastImage.priority.high,
          }}
          style={{
            height: imageHeight,
            width: imageWidth,
            borderTopLeftRadius: imageRadius,
            borderTopRightRadius: imageRadius,
            borderBottomLeftRadius:imageRadius,
            borderBottomRightRadius:imageRadius,
            backgroundColor: isDarkMode ? colors.whiteOpacity22 : colors.white,
            ...imageStyle,
          }}
          imageStyle={{
            borderRadius: moderateScale(10),
            backgroundColor: isDarkMode
              ? colors.whiteOpacity15
              : colors.greyColor,
          }}></FastImage>
        {/* <TouchableOpacity
          style={{
            position: 'absolute',
            top: 10,
            right: 30
          }}
          hitSlop={touchableHitSlopProp}
          onPress={() => _onAddtoWishlist((item))}>
          <View>
            {item?.inwishlist ? (
              <Image
                style={{
                  tintColor: isDarkMode
                    ? MyDarkTheme.colors.text
                    : themeColors.primary_color,
                }}
                source={imagePath.whiteFilledHeart}
              />
            ) : (
              <Image
                style={{
                  tintColor: isDarkMode
                    ? MyDarkTheme.colors.text
                    : themeColors.primary_color,
                }}
                source={imagePath.heart2}
              />
            )}
          </View>
        </TouchableOpacity> */}
      </View>

      <View style={{marginVertical: moderateScaleVertical(8)}}>
        <View
          style={{
            alignSelf: 'flex-start',
            marginBottom: moderateScaleVertical(4),
          }}>
          {!!item?.averageRating && item?.averageRating !== '0.0' && (
            <View style={styles.hdrRatingTxtView}>
              <Text
                style={{
                  ...styles.ratingTxt,
                  fontFamily: fontFamily.medium,
                }}>
                {Number(item?.averageRating).toFixed(1)}
              </Text>
              <Image
                style={styles.starImg}
                source={imagePath.star}
                resizeMode="contain"
              />
            </View>
          )}
        </View>
        <Text
          numberOfLines={numberOfLines}
          style={{
            fontSize: textScale(13),
            fontFamily: fontFamily.medium,
            color: isDarkMode ? MyDarkTheme.colors.text : colors.black,
            textAlign: 'center',
            // marginLeft: moderateScale(8),
          }}>
          {item?.title}
        </Text>

        {/* {!!item?.vendor_name ? <Text
          numberOfLines={1}
          style={{
            fontSize: textScale(12),
            fontFamily: fontFamily.regular,
            color: isDarkMode ? MyDarkTheme.colors.text : colors.blackOpacity66,
            textAlign: 'center',
            // marginLeft: moderateScale(8),
            marginBottom: moderateScaleVertical(4)
          }}>
          {item?.vendor_name || ''}
        </Text> : null} */}
        {!item?.hasOwnProperty('compare_price_numeric') ||
        Number(item?.compare_price_numeric) == 0 ? (
          <View
            style={{
              flexDirection: 'row',
              marginHorizontal: moderateScale(8),
              justifyContent: 'center',
              alignContent: 'center',
              flex: 1,
            }}>
            {priceType !== 'freelancer' && (
              <Text
                style={{
                  fontSize: textScale(10),
                  textAlign: 'center',
                  // fontFamily: fontFamily.bold,
                  color: isDarkMode
                    ? MyDarkTheme.colors.text
                    : themeColors.primary_color,
                }}>
                {tokenConverterPlusCurrencyNumberFormater(
                  item?.price_numeric,
                  digit_after_decimal,
                  additional_preferences,
                  currencies?.primary_currency?.symbol,
                  currencies,
                )}
              </Text>
            )}
            {!!category?.category_detail?.translation[0]?.name && (
              <Text
                numberOfLines={2}
                style={{
                  fontSize: textScale(10),
                  fontFamily: fontFamily.regular,
                  marginLeft: moderateScale(4),
                  flex: 1,
                  color: isDarkMode
                    ? MyDarkTheme.colors.text
                    : colors.blackOpacity66,
                }}>
                {category?.category_detail?.translation[0]?.name || category}
              </Text>
            )}
          </View>
        ) : (
          <View>
            {!!category?.category_detail?.translation[0]?.name && (
              <Text
                style={{
                  ...styles.inTextStyle,
                  fontFamily: fontFamily.regular,
                  color: isDarkMode
                    ? MyDarkTheme.colors.text
                    : colors.blackOpacity66,
                  marginLeft: moderateScale(5),
                  marginVertical: moderateScaleVertical(2),
                }}>
                {strings.IN} {category?.category_detail?.translation[0]?.name}
              </Text>
            )}
            {priceType !== 'freelancer' && (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginLeft: moderateScale(9),
                  // flexWrap: 'wrap',
                }}>
                <Text
                  style={{
                    fontSize: textScale(10),
                    fontFamily: fontFamily.medium,
                    color: colors.green,
                  }}>
                  {tokenConverterPlusCurrencyNumberFormater(
                    item?.price_numeric,
                    digit_after_decimal,
                    additional_preferences,
                    currencies?.primary_currency?.symbol,
                  )}
                </Text>
                <Text
                  numberOfLines={2}
                  style={{
                    fontSize: textScale(10),
                    fontFamily: fontFamily.medium,
                    textDecorationLine: 'line-through',
                    color: isDarkMode
                      ? MyDarkTheme.colors.text
                      : colors.blackOpacity40,
                    marginLeft: moderateScale(12),
                  }}>
                  {tokenConverterPlusCurrencyNumberFormater(
                    item?.compare_price_numeric,
                    digit_after_decimal,
                    additional_preferences,
                    currencies?.primary_currency?.symbol,
                  )}
                </Text>
              </View>
            )}
          </View>
        )}
      </View>
      {priceType !== 'freelancer' && !!Number(item?.compare_price_numeric) ? (
        <View
          style={{
            position: 'absolute',
            backgroundColor: themeColors?.primary_color,
            paddingVertical: moderateScaleVertical(4),
            paddingHorizontal: moderateScale(6),
            borderTopLeftRadius: moderateScale(6),
            borderBottomRightRadius: moderateScale(10),
            alignContent:'flex-start',
            alignItems:'flex-start',
            alignSelf:"flex-start"
          }}>
          <Text
            style={{
              fontSize: textScale(11),
              color: colors.white,
              fontFamily: fontFamily.medium,
            }}>
            {parseInt(
              (
                ((item?.compare_price_numeric - item?.price_numeric) /
                  item?.price_numeric) *
                100
              ).toFixed(3),
            )}
            % OFF
          </Text>
        </View>
      ) : null}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  hdrRatingTxtView: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.green,
    paddingVertical: moderateScale(2),
    paddingHorizontal: moderateScale(4),
    borderTopRighttRadius: moderateScale(6),
    // borderBottomLeftRadius: moderateScale(10),

    // marginTop: moderateScaleVertical(16),
  },
  ratingTxt: {
    textAlign: 'left',
    color: colors.white,
    fontSize: textScale(9),
    textAlign: 'left',
  },
  starImg: {
    tintColor: colors.white,
    marginLeft: 2,
    width: 9,
    height: 9,
  },
  inTextStyle: {
    fontSize: textScale(9),
    width: width / 3,
    textAlign: 'left',
  },
});

export default React.memo(ProductsComp);

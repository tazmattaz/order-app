import { View, Text, StyleSheet, Image } from "react-native";
import React, { useEffect, useState } from "react";
import * as Animatable from "react-native-animatable";
import Carousel, { Pagination } from "react-native-snap-carousel";
import {
  height,
  moderateScale,
  moderateScaleVertical,
  textScale,
  width,
} from "../styles/responsiveSize";
import { getImageUrl } from "../utils/helperFunctions";
import { TouchableOpacity } from "react-native";
import FastImage from "react-native-fast-image";
import colors from "../styles/colors";
import { black } from "react-native-paper/lib/typescript/styles/colors";

import imagePath from "../constants/imagePath";
import { isArray, isEmpty } from "lodash";
import commonStylesFunc, { hitSlopProp } from "../styles/commonStyles";
import { useSelector } from "react-redux";
import { tokenConverterPlusCurrencyNumberFormater } from "../utils/commonFunction";
import strings from "../constants/lang";
import { UIActivityIndicator } from "react-native-indicators";
import { MyDarkTheme } from "../styles/theme";
import RenderHTML from "react-native-render-html";

const ProductCardRedThread = ({
  data = {},
  onPress = () => { },
  addToCart = () => { },
  index,
  onIncrement,
  onDecrement,
  selectedItemID,
  Servicetype,
  isVisibleModal,
  selectedItemIndx,
  btnLoader,
  categoryInfo = "",
  businessType,
  animateText = 0,
  section = {},
  CartItems = {},
}) => {
  const { appStyle, themeColors, appData } = useSelector(
    (state) => state?.initBoot
  );
  const [readmore, setreadmore] = useState(false)

  const dine_In_Type = useSelector((state) => state?.home?.dineInType);

  const theme = useSelector((state) => state?.initBoot?.themeColor);
  console.log(data, "nsdjhsdhfjghjg");
  let plainHtml = !!data?.translation_description
    ? data?.translation_description.toString()
    : !!data?.translation[0]?.translation_description
      ? data?.translation[0]?.translation_description
      : '';


  useEffect(() => {

    if (!isEmpty(data?.check_if_in_cart_app)) {
      data?.check_if_in_cart_app.map((val) => {
        var totalProductQty = 0;
        totalProductQty = val.quantity;
        console.log(totalProductQty, 'totalProductQty')
        updateState({ qtyText: totalProductQty });
      });
    }
  }, [data]);


  const [isPressing, setIsPressing] = useState(false);

  const handlePressIn = () => {
    setIsPressing(true);
  };

  const handlePressOut = () => {
    setIsPressing(false);
  };
  const { additional_preferences, digit_after_decimal } =
    appData?.profile?.preferences || {};
  const currencies = useSelector((state) => state?.initBoot?.currencies);

  const isDarkMode = theme;
  const url1 = !isEmpty(data?.media) && data?.media[0]?.image?.path.image_fit;
  const url2 = !isEmpty(data?.media) && data?.media[0]?.image?.path.image_path;
  const getImage = (quality) => getImageUrl(url1, url2, quality);

  const fontFamily = appStyle?.fontSizeData;
  const styles = styleData({ themeColors, fontFamily });
  const commonStyles = commonStylesFunc({ fontFamily });
  const [isAdd, setAdd] = useState(false);
  const [state, setState] = useState({
    selectedIndex: -1,
    selectedIndexForCartIcon: -1,
    isVisibleTextSlideUp: false,
    qtyText: "1",
    isVisibleText: true,
    disabledBtn: false,
    isIncrement: true,
  });
  const {
    selectedIndex,
    selectedIndexForCartIcon,
    isVisibleText,
    qtyText,
    isVisibleTextSlideUp,
    disabledBtn,
    isIncrement,
  } = state;

  const updateState = (data) => setState((state) => ({ ...state, ...data }));

  const onIncrementQty = () => {
    setAdd(true);
    if (
      !!categoryInfo?.is_vendor_closed &&
      categoryInfo?.closed_store_order_scheduled !== 1
    ) {
      alert(strings.VENDOR_NOT_ACCEPTING_ORDERS);
      return;
    }
    if (
      (!!data?.add_on_count && data?.add_on_count !== 0) ||
      (!!data?.variant_set_count && data?.variant_set_count !== 0)
    ) {
      onIncrement();
    } else {
      updateState({ ...state, isIncrement: true });
      // if (!disabledBtn) {
      //   const isEnabled = numberOfHits.length === 0;
      //   numberOfHits.push(data?.qty || totalProductQty);
      //   if (isEnabled) {
      //     initAnimation();
      //   }
      // }
      onIncrement();
    }
  };
  const onDecrementQty = () => {
    setAdd(false);
    if (
      (!!data?.add_on_count && data?.add_on_count !== 0) ||
      (!!data?.variant_set_count && data?.variant_set_count !== 0)
    ) {
      onDecrement();
    } else {
      updateState({ ...state, isIncrement: false });
      // if (!disabledBtn) {
      //   const isEnabled = numberOfHits.length === 0;
      //   numberOfHits.push(data?.qty || totalProductQty);
      //   if (isEnabled) {
      //     initAnimation();
      //   }
      // }
      onDecrement();
    }
  };
  const renderProductImages = ({ item, index }) => {
    return (
      <Image
        source={item.image}
        style={{ width: width, height: height / 1.5 }}
      />
    );
  };


  return (
    <TouchableOpacity

      activeOpacity={0.9}
      onPress={onPress}
      style={{
        // borderWidth: 1,
        // borderColor: "black",
        marginVertical: moderateScaleVertical(12),
      }}
    >
      <View
        style={{

        }}
      >
        {!!url1 ? (
          <FastImage
            style={{
              ...styles.imgStyle,
              backgroundColor: isDarkMode
                ? colors.whiteOpacity15
                : colors.greyColor,
              borderRadius: moderateScale(7),
              height: moderateScaleVertical(503),
              marginBottom: 10
              // aspectRatio: 1,
            }}
            source={{
              uri: getImage('800/800'),
              cache: FastImage.cacheControl.immutable,
              priority: FastImage.priority.high,
            }}
          />
        ) : (
          <View
            style={{
              ...styles.imgStyle,
              backgroundColor: isDarkMode
                ? colors.whiteOpacity15
                : colors.greyColor,
              borderRadius: moderateScale(7),
            }}
          />
        )}
      </View>
      <View style={{ flex: 1, marginHorizontal: moderateScale(20), flexDirection: 'row', alignItems: 'center', marginBottom: moderateScaleVertical(6), justifyContent: 'space-between' }}>
        <Text
          style={{
            ...commonStyles.futuraBtHeavyFont14,
            color: isDarkMode ? colors.white : colors.black,
            fontFamily: fontFamily.medium,
            fontSize: textScale(14),
            flex: 0.5
          }}
        >
          {!isEmpty(data?.translation)
            ? data?.translation[0]?.title
            : data?.title || data?.sku}
        </Text>
        {/* Price view */}
        <View
          style={{
            paddingTop: moderateScale(5),
            paddingBottom: moderateScale(5),
            flexDirection: "row",
            justifyContent: 'flex-end',
            alignItems: 'center',
            marginRight: 2,
            flex: 0.5
          }}
        >
          <Text
            numberOfLines={1}
            style={{
              ...commonStyles.mediumFont14,
              color: themeColors?.primary_color,
              fontSize: textScale(13),
              fontFamily: fontFamily.medium,
              marginRight: moderateScale(6)
            }}

          >
            {tokenConverterPlusCurrencyNumberFormater(
              Number(data?.variant[0]?.price) *
              Number(data?.variant[0]?.multiplier || 1),
              digit_after_decimal,
              additional_preferences,
              currencies?.primary_currency?.symbol,
              currencies
            )}
          </Text>

          {Number(data?.variant[0]?.compare_at_price) >
            Number(data?.variant[0]?.price) && !!data?.variant[0]?.compare_at_price && (
              <Text
                numberOfLines={1}
                style={{
                  ...commonStyles.mediumFont14,
                  color: isDarkMode ? colors.white : colors.redB,
                  fontSize: textScale(13),
                  fontFamily: fontFamily.regular,
                  textDecorationLine: "line-through",
                }}
              >
                {tokenConverterPlusCurrencyNumberFormater(
                  Number(data?.variant[0]?.compare_at_price) *
                  Number(data?.variant[0]?.multiplier || 1),
                  digit_after_decimal,
                  additional_preferences,
                  currencies?.primary_currency?.symbol,
                  currencies
                )}
              </Text>
            )}
        </View>
      </View>
      <View style={{
        marginHorizontal: moderateScale(20),
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignItems: 'center',
        alignContent: 'center',

      }}>
        {!!data?.translation_description ||
          !!data?.translation[0]?.translation_description ?
          <View style={{
            alignItems: "flex-start",
            alignContent: "flex-start",

          }}>
            <Text
              numberOfLines={readmore == false ? 1 : 18}
              style={{
                ...commonStyles.mediumFont14,
                color: isDarkMode ? colors.white : colors.textGrey,
                width: width / 2,
                fontSize: textScale(10),
                // top: readmore == false ? -10 : 0,
                textAlign: "left",


              }}
            >{plainHtml.replace(/(&nbsp;|<([^>]+)>)/ig, '').trim()}
            </Text>

            {/* {console.log('plainnnht', plainHtml.replace(/<\/?p>|<\/?h\d>|<\/?pre>/g, "").length)} */}
            {plainHtml.replace(/(&nbsp;|<([^>]+)>)/ig, '').trim().length > 24 && <View>
              {plainHtml.replace(/<\/?p>|<\/?h\d>|<\/?pre>|\s+/g, ' ').length > 19 && <Text onPress={() => setreadmore(!readmore)}
                style={{
                  color: themeColors.primary_color,

                  backgroundColor: isDarkMode ? MyDarkTheme.colors.background :
                    colors.white,
                  zIndex: 1,
                  // position: !readmore == true ? 'absolute' : null,
                  right: 0, bottom: 0,
                  fontSize: textScale(10),
                  fontFamily: fontFamily.bold,
                  // top: readmore == false ? 0 : 0

                }}>{readmore == true ? 'Readless' : ' ...Readmore'} </Text>}
            </View>}
          </View> : <View />}
        <View>
          {data?.has_inventory == 0 ||
            !!data?.variant[0]?.quantity ||
            (!!typeId && typeId == 8) ||
            (!!businessType && businessType == "laundry") ? (
            <View
              style={{
                // marginTop:
                //   selectedIndex == index
                //     ? moderateScaleVertical(8)
                //     : moderateScaleVertical(8),
                alignItems: "center",
                justifyContent: 'flex-end',
              }}
            >
              {((!!data?.check_if_in_cart_app &&
                data?.check_if_in_cart_app.length > 0) ||
                !!data?.qty
              ) &&
                CartItems.data !== null &&
                dine_In_Type != "appointment" ? (
                <View
                  // pointerEvents={!!categoryInfo?.is_vendor_closed && !!categoryInfo?.closed_store_order_scheduled !== 1 ? 'none' : 'auto'}
                  style={{
                    ...styles.addBtnStyle,
                    paddingVertical: 0,
                    height: moderateScale(38),
                    backgroundColor: themeColors.primary_color,
                    alignItems: "center",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    borderRadius: moderateScale(8),
                    paddingHorizontal: moderateScale(12),
                    width: moderateScale(100),
                  }}
                >
                  <TouchableOpacity
                    disabled={selectedItemID == data?.id}
                    onPress={onDecrementQty}
                    activeOpacity={0.8}
                    hitSlop={hitSlopProp}
                  >
                    <Image
                      style={{
                        tintColor: colors.white

                      }}
                      source={imagePath.icMinus2}
                    />
                  </TouchableOpacity>

                  <Animatable.View>
                    {selectedItemID == data?.id && btnLoader ? (
                      <UIActivityIndicator
                        size={moderateScale(16)}
                        color={themeColors.primary_color}
                        style={{
                          marginHorizontal: moderateScale(8),
                        }}
                      />
                    ) : (
                      <Animatable.View style={{ overflow: "hidden" }}>
                        {isVisibleText ? (
                          <Animatable.Text
                            // key={String(animateText)}

                            // animation={isAdd ? 'slideInUp' : 'slideInDown'}
                            // easing={'ease-out-sine'}
                            // animation={
                            //   isIncrement
                            //     ? isVisibleTextSlideUp
                            //       ? textAnimateForIncrement
                            //       : textAnimateForIncrement_
                            //     : isVisibleTextSlideUp
                            //     ? textAnimateForDecrement
                            //     : textAnimateForDecrement_
                            // }
                            duration={200}
                            // delay={200}
                            numberOfLines={2}
                            style={{
                              fontFamily: fontFamily.medium,
                              fontSize: moderateScale(14),
                              color: colors.white,

                              // height: moderateScale(100),
                              marginHorizontal: moderateScale(8),
                            }}
                          >
                            {/* {qtyText || data?.qty || totalProductQty} */}
                            {qtyText}
                          </Animatable.Text>
                        ) : null}
                      </Animatable.View>
                    )}
                  </Animatable.View>
                  <TouchableOpacity
                    disabled={selectedItemID == data?.id}
                    activeOpacity={0.8}
                    // hitSlop={hitSlopProp}
                    onPress={onIncrementQty}

                  >
                    <Image
                      style={{
                        tintColor: colors.white
                        ,
                      }}
                      source={imagePath.icAdd4}
                    />
                  </TouchableOpacity>
                </View>
              ) : (
                <>
                  <TouchableOpacity
                    // hitSlopProp={hitSlopProp}
                    disabled={selectedItemID == data?.id}
                    onPress={addToCart}
                    style={{
                      // ...styles.addBtnStyle,
                      backgroundColor: themeColors.primary_color,
                      //   width: moderateScale(100),
                      // minHeight: moderateScaleVertical(35),
                      // marginTop: moderateScaleVertical(5),
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        marginVertical: moderateScaleVertical(8),
                        marginHorizontal: moderateScale(4),

                      }}
                    >
                      <Image
                        style={{
                          height: moderateScaleVertical(20),
                          width: moderateScale(20),
                          tintColor: colors.white,
                        }}
                        source={imagePath.cartIcon}
                      />
                      <Text
                        style={{
                          ...styles.addStyleText,
                          // color: ,
                        }}
                      >
                        {!!data?.check_if_in_cart_app &&
                          data?.check_if_in_cart_app.length > 0
                          ? strings.ADDED
                          : "Add to cart"}{" "}
                        {data?.minimum_order_count > 1
                          ? `(${data?.minimum_order_count})`
                          : ""}
                      </Text>
                    </View>

                    {/* <Image source={imagePath.greyRoundPlus} /> */}
                  </TouchableOpacity>
                </>
              )}
              {(!!data?.add_on_count && data?.add_on_count !== 0) ||
                (!!data?.variant_set_count && data?.variant_set_count !== 0) ||
                (!!isArray(data?.add_on) && data?.add_on?.length !== 0) ? (
                <Text
                  style={{
                    ...styles.customTextStyle,
                    textTransform: "lowercase",
                    color: isDarkMode ? colors.white : colors.blackOpacity40,
                  }}
                >
                  {strings.CUSTOMISABLE}
                </Text>
              ) : null}
            </View>
          ) : (
            <Text style={styles.outOfStock}>{strings.OUT_OF_STOCK}</Text>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};
function styleData({ themeColors, fontFamily }) {
  const styles = StyleSheet.create({
    outOfStock: {
      color: colors.orangeB,
      fontSize: textScale(10),
      lineHeight: 20,
      fontFamily: fontFamily.medium,
    },
    customTextStyle: {
      fontSize: textScale(8),
      color: themeColors.primary_color,
      fontFamily: fontFamily.medium,
      marginTop: moderateScaleVertical(4),
      color: colors.yellowC,
    },
    addStyleText: {
      fontSize: textScale(10),
      color: colors.white,
      fontFamily: fontFamily.bold,
      marginLeft: moderateScale(6),
    },
    addBtnStyle: {
      // borderWidth: 1,
      // paddingVertical: moderateScaleVertical(6),
      // borderRadius: moderateScale(8),
      borderColor: themeColors.primary_color,
      // justifyContent: 'center',
      // alignItems: 'center',
      // width: moderateScale(100),
      // minHeight: moderateScaleVertical(35),
      // flexDirection:"row"
      // width: moderateScale(80),
    },
    inTextStyle: {
      width: moderateScaleVertical(220),
      fontFamily: fontFamily.regular,
      fontSize: textScale(9),
      width: width / 3,
      textAlign: "left",
      marginTop: moderateScaleVertical(6),
      marginBottom: moderateScaleVertical(4),
    },
    imgStyle: {
      width: width,
      height: height / 1.5,
      borderRadius: moderateScale(15),
    },
  });
  return styles;
}
export default React.memo(ProductCardRedThread);
